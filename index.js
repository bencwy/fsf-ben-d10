var express = require("express");
var app = express();

app.get("/processForm",function(req,res){
    var data = JSON.parse(req.query.formDetails);
    console.log(data);
    res.status(202);
    res.end();
});

app.use(express.static(__dirname+"/public"));
app.use("/libs",express.static(__dirname+"/bower_components"));

var port = process.argv[2] || 3000;
app.listen(port,function(){
    console.log("App started on port " + port);
});