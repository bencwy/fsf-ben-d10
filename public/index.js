(function(){
    var RegApp = angular.module("RegApp", ["countrySelect"]);

    var RegCtrl = function($http){
        var regCtrl = this;
        regCtrl.init = function(){
            regCtrl.name = "";
            regCtrl.email = "";
            regCtrl.password = "";
            regCtrl.passwordFeedback = "";
            regCtrl.gender = "";
            regCtrl.dob = "";
            regCtrl.country = "";
            regCtrl.contact = "";
            regCtrl.submitted = "Thank You!";
            regCtrl.address = "";
        }

        regCtrl.process = function(){
            // console.log("name : " + regCtrl.name);
            // console.log("email : " + regCtrl.email);
            // console.log("password : " + regCtrl.password);
            // console.log("gender : " + regCtrl.gender);
            // console.log("dob : " + regCtrl.dob);
            // console.log("country : " + regCtrl.country);
            // console.log("contact : " + regCtrl.contact);
            $http.get("/processForm",{
                params: {
                    formDetails:{
                        name: regCtrl.name,
                        email: regCtrl.email,
                        password: regCtrl.password,
                        gender: regCtrl.gender,
                        dob: regCtrl.dob,
                        country: regCtrl.country,
                        contact: regCtrl.contact,
                        address: regCtrl.address
                    }
                }
            })
        }

        regCtrl.checkdob = function(){
            var yearPlus18 = regCtrl.dob.getFullYear() + 18;
            var month = regCtrl.dob.getMonth() - 1; // New date month 0-11
            var day = regCtrl.dob.getDate();
            var birthPlus18 = new Date(yearPlus18,month,day);
            if (birthPlus18 > new Date()) {
                return false;
            }else{
                return true;
            }
        }

        regCtrl.checkpassword = function(){
            if(regCtrl.password == undefined){
                return false;
            }
            var feedback = "";
            var pass = true;
            if(regCtrl.password.length){
                if (regCtrl.password.length < 8) {
                    feedback += "Too short, ";
                    pass = false;
                }
                if (regCtrl.password.search(/[0-9]/) == -1) {
                    feedback += "No number, ";
                    pass = false;
                }
                if (regCtrl.password.search(/[a-z]/) == -1) {
                    feedback += "No lowercase letter, ";
                    pass = false;
                }
                if (regCtrl.password.search(/[A-Z]/) == -1) {
                    feedback += "No uppercase letter, ";
                    pass = false;
                }
                if (regCtrl.password.search(/[@#$]/) == -1) {
                    feedback += "No special character @ # $, ";
                    pass = false;
                }
                regCtrl.passwordFeedback = feedback.substring(0,feedback.length-2);
                return pass;
            }
        }

        regCtrl.checkcontact = function(){
            if ((/[^+\- ()a-zA-Z0-9]/).test(regCtrl.contact)){
                return false;
            }
            return true;
        }

        regCtrl.init();
    };

    RegCtrl.$inject = ["$http"];

    RegApp.controller("RegCtrl", RegCtrl);
})();